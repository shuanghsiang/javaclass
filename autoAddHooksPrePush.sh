#!/usr/bin/env bash
mkdir -p ./.git/hooks
if [[ ! -f ./.git/hooks/pre-push ]]; then
echo "ADD ./.git/hooks/pre-push"
cp ./pre-push ./.git/hooks/pre-push
exit 0
fi